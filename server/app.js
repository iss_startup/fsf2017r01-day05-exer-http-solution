// TODO: 1. Create barebone Web app server
// TODO: 2. Set up client directory to serve static files
// TODO: 3. Handle errors
// TODO: 4. Handle request for /showreq

// TODO: 1.1 Load/import the express module required by this application
// Loads express module and assigns it to a var called express
var express = require("express");

// TODO: 1.2 Load/import the path module required by this application
// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// TODO: 2.1 Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');  // CLIENT FOLDER is the public directory
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// TODO: 1.3 Define Server Port
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 3000;

// TODO: 1.4 Invoke an instance of express and assign it to a variable called app
// Creates an instance of Express
var app = express();

// TODO: 2.2 Serve static files
/* Serves files from public directory
 __dirname is the absolute path of the application directory
 */
app.use(express.static(CLIENT_FOLDER));


// TODO: 4.1 Define /showreq route handler
// TODO: 4.2 Read the request object and display information from the request header
// TODO: 4.3 Respond with the message "Reached /showreq"
app.get('/showreq', function(req, res){
    console.log("\nHost: " + req.get('Host'));
    console.log("Content type required by client: " + req.get('Accept'));
    console.log("Language required by client: " + req.get('Accept-Language'));
    console.log("User Agent: " + req.get('User-Agent'));

    res.status(200).send("Reached /showreq");
});

// TODO: 3.1 Handle  Missing Resource Errors (404)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// TODO: 3.2 Handle Server Errors (501)
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

// TODO: 1.5 Start server and have it listen on Node Port
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});